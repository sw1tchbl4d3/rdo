CFLAGS = -Wall -Wextra -Werror -Wl,-z,now
CFLAGS_RELEASE = ${CFLAGS} -O2 -s -D_FORTIFY_SOURCE=2
CFLAGS_DEBUG = ${CFLAGS} -O0 -g -fsanitize=undefined
CFLAGS_STATIC = ${CFLAGS_RELEASE} -static-pie
LIBS = -lcrypt
CC = gcc

all: rdo.c
	${CC} ${CFLAGS_RELEASE} rdo.c -o rdo ${LIBS}

static: rdo.c
	${CC} ${CFLAGS_STATIC} rdo.c -o rdo ${LIBS}

debug: rdo.c
	${CC} ${CFLAGS_DEBUG} rdo.c -o rdo ${LIBS}

install: rdo
	cp rdo ${DESTDIR}/usr/local/bin/rdo
	chown 0:0 ${DESTDIR}/usr/local/bin/rdo
	chmod 755 ${DESTDIR}/usr/local/bin/rdo
	chmod u+s ${DESTDIR}/usr/local/bin/rdo
	cp rdo_sample.conf ${DESTDIR}/etc/rdo.conf
	chmod 600 ${DESTDIR}/etc/rdo.conf

uninstall:
	rm /usr/local/bin/rdo
	rm /etc/rdo.conf

clean:
	rm rdo

bench-clean:
	docker rmi -f rdo-benchmark

bench-build: bench-clean
	docker buildx build -t rdo-benchmark -f benchmark/Dockerfile .

bench-run:
	docker run --rm -t rdo-benchmark
