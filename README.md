# RootDO [![AUR](https://img.shields.io/aur/version/rdo.svg)](https://aur.archlinux.org/packages/rdo/)

This project aims to be a very slim alternative to both sudo and doas.

### Installation

If you are on Arch Linux, you can download the package via the [AUR](https://aur.archlinux.org/packages/rdo/).

You can clone and build rdo with the following set of commands:

```sh
git clone https://codeberg.org/sw1tchbl4d3/rdo
cd rdo
make
sudo make install
```

After that, you'll have to configure rdo to allow you to use it.
To do this, edit `/etc/rdo.conf`, and set the group variable to the admin group you are in.

Then you're good to go!

To uninstall:
```sh
sudo make uninstall
```

### Usage

```sh
rdo [command]
```

Or, to get the password from stdin:

```sh
rdo - [command]
```

The configuration file has the following variables:
```
group=wheel
wrong_pw_sleep=1000
session_ttl=5
```

- `group`: The group of users that is allowed to execute rdo.
- `wrong_pw_sleep`: The amount of milliseconds to sleep at a wrong password attempt. Must be a positive integer. Set to 0 to disable.
- `session_ttl`: The amount of minutes a session lasts. Must be a positive integer. Set to 0 to disable.

### Benchmarks

The benchmark: Execute `whoami` (GNU coreutils 9.1) 10000 times.

Yes, this is a silly benchmark. Yes, the performance gain in real world application is close to nothing.

But it's fun!

|Program|Time|
--- | --- 
sudo 1.19.11 | 46.85s
doas 6.8.2 | 32.57s
rdo 1.4.2 | 13.37s
Baseline | 7.95s

> Baseline here is how long it took without any wrapper to make it root.

These benchmarks were done on a `Intel i5 7200U` processor, on a Debian 12 Docker container.

`sudo` and `doas` were pulled from the Debian repos, `rdo` was compiled locally.

All configs were kept as default, except allow the `wheel` group on both + enable `persist` on doas.

The benchmark can be executed through a Docker container by running:

```
make bench-build bench-run
```
